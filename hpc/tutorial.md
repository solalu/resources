## Petsc

#### Installation
NOTA: 
MPI debe estar instalado (en caso contrario agregar --download-mpich o --download-openmpi en ./configure)
y compilar con gcc, gxx y gfortran
```bash
git clone -b maint https://gitlab.com/petsc/petsc.git petsc
cd petsc
# Install valgrind-devel
# Archlinux: sudo pacman -S valgrind-devel
# Centos: sudo yum -y install valgrind-devel
./configure --with-cc=mpicc --with-cxx=mpicxx --with-fc=mpif90 --download-make --download-cmake --download-fblaslapack --download-scalapack --download-mumps --download-superlu_dist --download-hypre
```
Luego seguir las intrucciones de compilación


### MPI
* [MPI complete tutorials](https://mpitutorial.com/tutorials/)
