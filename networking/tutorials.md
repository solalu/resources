#### Networking learning
* [Complete coutse for CCNA Cisco exam](https://www.youtube.com/watch?v=K6Qt23sY68Y&list=PLxbwE86jKRgMpuZuLBivzlM8s2Dk5lXBQ&index=5)

#### GNS - Networking tool for simulate networks (configuration of switches and nodes)
* [Basic course(videos) of GNS](https://www.youtube.com/watch?v=JhXUNRO8Vac&list=PLy07KpesUR00svxy0eOkjL4L3ymOXmvzf&index=7)
* [GNS Installation on GNU/Linux](https://docs.gns3.com/1QXVIihk7dsOL7Xr7Bmz4zRzTsJ02wklfImGuHwTlaA4/)
* [Cisco IOS images for Dynamips](https://docs.gns3.com/1-kBrTplBltp9P3P-AigoMzlDO-ISyL1h3bYpOl5Q8mQ/index.html#h.bi4322gmx9yl)

#### Repository of linux packages
* [Repository of linux packages](https://pkgs.org/)


#### NFS server
* [Configuration of NFS server in centos 8](https://linuxhint.com/configure_nfs_server_centos8/)

#### DHCP Server
* [Configuration of a DHPC sever in OS based in Debian](https://www.tecmint.com/install-dhcp-server-in-ubuntu-debian/amp/)
* [Configuration of a DHPC server in Centos](https://kifarunix.com/install-and-setup-dhcp-server-on-centos-8/)

#### Setup a Static ip4
* [Configuration of a static ip adress](https://www.tecmint.com/set-add-static-ip-address-in-linux/)


#### Ganglia
* [Configuration of ganglia in centos](https://www.youtube.com/watch?v=tjHBYp649Ps&t=381s)


#### Cluster configuration
* [Building a cluster with Raspberry pi](https://medium.com/@glmdev/building-a-raspberry-pi-cluster-784f0df9afbd)

#### KVM
* [Installation of KVM in centos](https://www.cyberciti.biz/faq/how-to-install-kvm-on-centos-7-rhel-7-headless-server/)
