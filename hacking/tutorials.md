#### John The Ripper

* [John The Ripper with parallel distributed computing](https://louwrentius.com/parallel-distributed-password-cracking-with-john-the-ripper-and-mpi.html)
* [John The Ripper Examples](https://www.openwall.com/john/doc/EXAMPLES.shtml)
* [John the Ripper tutorial](https://www.hackingarticles.in/beginner-guide-john-the-ripper-part-1/)
* [John the Ripper (github)](https://github.com/magnumripper/JohnTheRipper)


###### Intructions for installing John The ripper in 
* Download the source code of John the ripper , here [github john the ripper](https://www.liquidweb.com/kb/enable-root-login-via-ssh/)

* Building John The Ripper
```bash
cd UNTARED_JOHN_RIPPER_DIR/src
./configure --prefix=/usr --with-systemwide --enable-mpi --enable-opencl --enable-pkg-config
make

cp -r jrt/* /usr/share/john
```

* Finally add your the path of `run` directory to your global path (/etc/bashrc)


#### Metasploit
* [Basic Commands](https://www.offensive-security.com/metasploit-unleashed/meterpreter-basics/)
* [Metasploit installation in centos](https://computingforgeeks.com/install-metasploit-framework-on-centos-linux/)

#### Ruby
* [Ruby Tutorial](https://www.tutorialspoint.com/ruby/ruby_classes.htm)

#### Customizing Prompt
* [Parrot Prompt](https://github.com/ParrotSec/parrot-core/blob/master/parrot-core/root/.bashrc)

#### Social Engineer Toolkit
* [SET github repo](https://github.com/trustedsec/social-engineer-toolkit)

#### Android
* [termux-sudo (gitlab)](https://gitlab.com/st42/termux-sudo)
* [tsu (github)](https://github.com/cswl/tsu)
* [texmux organization (github)](https://github.com/termux)
* [Android emulator archlinux](https://aur.archlinux.org/packages/android-emulator/)
