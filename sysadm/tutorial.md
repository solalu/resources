#### Gparted - graphical tools for create a manipulate disks
*[Gparted live iso](https://sourceforge.net/projects/gparted/)

#### OS
* [Windows server isos (pruebas)](https://www.microsoft.com/en-us/evalcenter/evaluate-windows-server-2019)

#### vim
* [vim tutorial](https://www.tutorialspoint.com/vim/vim_using_vim_as_ide.htm)
* [vim autocompletation](https://github.com/ycm-core/YouCompleteMe#linux-64-bit)
* [ycm_extra_conf.py](https://github.com/rasendubi/dotfiles/blob/master/.vim/.ycm_extra_conf.py)
* [vundle](https://github.com/VundleVim/Vundle.vim)
* [Configuration od YouCompleteMe with other languages](https://github.com/ycm-core/lsp-examples)

#### Bash
* [Short bash syntaxis guide](https://devhints.io/bash)
